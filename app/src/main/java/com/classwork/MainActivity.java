package com.classwork;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import java.io.ByteArrayOutputStream;

public class MainActivity extends AppCompatActivity {
    Button carImage, bleachImage, kenpahiImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Bundle extras = new Bundle();
        carImage = (Button) findViewById(R.id.car_image);
        carImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, ImageActivity.class);
                intent.putExtra("image", R.drawable.image);
                startActivity(intent);
            }
        });

        bleachImage = (Button) findViewById(R.id.bleach_image);
        bleachImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ImageActivity.class);
                intent.putExtra("image", R.drawable.kenny);
                startActivity(intent);
            }
        });
        kenpahiImage = (Button) findViewById(R.id.kenpachi_image);
        kenpahiImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ImageActivity.class);
                intent.putExtra("image", R.drawable.vader);
                startActivity(intent);
            }
        });

    }

}
