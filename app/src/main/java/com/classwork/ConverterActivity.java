package com.classwork;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

public class ConverterActivity extends AppCompatActivity {

    private EditText celsiusView, fahrView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_converter);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        celsiusView = (EditText)findViewById(R.id.celsius);
        fahrView = (EditText)findViewById(R.id.fahr);

        Button converter_button = (Button) findViewById(R.id.convert_button);
        converter_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Convert();
            }
        });
    }

    @SuppressLint("SetTextI18n")
    protected void Convert(){
        String celsius =celsiusView.getText().toString();
        String fahr = fahrView.getText().toString();

        int temp;
        if(TextUtils.isEmpty(fahr)){
            int doubleCelsius = Integer.parseInt(celsius);
            temp = (int) ((doubleCelsius *1.8) +32);
            fahrView.setText(Integer.toString(temp));
        }
        if (TextUtils.isEmpty(celsius)){
            int intFahr = Integer.parseInt(fahr);
            temp = ((intFahr - 32)*5)/9;
            celsiusView.setText(Integer.toString(temp));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_converter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
